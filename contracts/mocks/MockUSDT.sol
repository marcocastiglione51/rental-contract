// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";

contract MockUSDT is ERC20, ERC20Burnable {
    // solhint-disable-next-line
    constructor() ERC20("Mock USDT", "MUSDT") {
        _mint(msg.sender, 10000000000 * 10**decimals());
    }

}
