// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;

import "@openzeppelin/contracts/proxy/Clones.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "./SyntheticNFT.sol";
import "./Interfaces.sol";
import "./Structs.sol";
import "./Vault.sol";
import "./governance/ProtocolParameters.sol";
import "hardhat/console.sol";

contract RentalManager is OwnableUpgradeable, UUPSUpgradeable {

    event OfferListed(
        address collection,
        uint256 tokenId,
        uint256 maximumRentTime,
        uint256 pricePerSecond,
        uint256 rentalExpiration,
        address fundingToken,
        address owner,
        bytes32 rentalId
    );

    event OfferCanceled(
        bytes32 rentalId
    );

    event RentalOffered(
        address collection,
        uint256 tokenId,
        uint256 rentalTime,
        uint256 pricePerSecond,
        uint256 rentalExpiration,
        address fundingToken,
        address operator,
        bytes32 rentalOfferId
    );

    event OfferAccepted(
        bytes32 offerId
    );

    event OfferDeclined(
        bytes32 rentalId
    );

    event NFTRented(
        address collection,
        uint256 tokenId,
        uint256 rentalTime,
        uint256 pricePerSecond,
        uint256 rentalExpiration,
        address fundingToken,
        address operator,
        uint256 syntheticID
    );

    /// @dev Listing ID => offer (seller)
    mapping(bytes32 => RentalListing) private _rentalSellOrders;

    /// @dev collection - token ID - rental expiration
    mapping(address => mapping(uint256 => uint256)) public rentalExpirations;

    /// @dev Listing ID => offer (buyer)
    mapping (bytes32 => RentalOffer) private _rentalBuyOrders;

    /// @dev collection => synthetic collection
    mapping (address => address) private _collections;

    /// @dev funding token => allowed or not
    mapping (address => bool) public whitelistedFundingTokens;

    /// @dev Synthetic NFT address (before initialization)
    address private _syntheticNFT;

    /// @dev Vault for original NFTs
    address public vaultAddress;

    address private _protocolParameters;

    /// @dev collection - token ID - RentedNFT data
    mapping(address => mapping(uint256 => RentalListing)) public rentedTokenData;

    /// @dev collection - token ID - synthetic id
    mapping(address => mapping(uint256 => uint256)) public rentedTokenSyntheticID;

    event RentalOfferCanceled(
        bytes32 rentalId
    );

    constructor() initializer {}

    function initialize(
        address syntheticNFT_,
        address vaultAddress_,
        address protocolParameters_
    ) external initializer {
        __Ownable_init();
        __UUPSUpgradeable_init();
        _syntheticNFT = syntheticNFT_;
        vaultAddress = vaultAddress_;
        _protocolParameters = protocolParameters_;
    }

    function whitelistFundingToken(address fundingToken) public onlyOwner {
        whitelistedFundingTokens[fundingToken] = !whitelistedFundingTokens[fundingToken];
    }

    /**
    * @notice this is the owner listing his NFT for rental.
    * If the collection is new, mints a new rentalSyntheticNFT contract.
    */
    function listOffer(
        address collection_,
        uint256 tokenId_,
        uint256 maximumRentTime_,
        uint256 pricePerSecond_,
        uint256 rentalExpiration_,
        address fundingToken_
    ) public {

        // check if is the token owner
        require(maximumRentTime_ > 0, "Rent time must be greater than zero");
        require(pricePerSecond_ > 0, "pricePerSecond must be greater than zero");
        require(rentalExpiration_ > block.timestamp, "rentalExpiration must be greater than block timestamp!");
        require(whitelistedFundingTokens[fundingToken_], "Funding token must be whitelisted!");

        address approved = IERC721Metadata(collection_).getApproved(tokenId_);
        
        require(approved == address(this), "Token must be approved");

        require(!isTokenRented(collection_, tokenId_), "Token already rented!");

        bytes32 rentalId = getRentalSellID(
            collection_,
            tokenId_,
            maximumRentTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            msg.sender
        );

        if (_collections[collection_] == address(0)) {
            //Deploy rentalSyntheticNFT

            IERC721Metadata originalCollection = IERC721Metadata(collection_);

            address syntheticNFTAddress = Clones.clone(_syntheticNFT);

            string memory originalName = originalCollection.name();

            string memory originalSymbol = originalCollection.symbol();

            // initializes the SyntheticNFT
            SyntheticNFT(syntheticNFTAddress).initialize(
                string(abi.encodePacked("Rented ", originalName)),
                string(abi.encodePacked("r", originalSymbol)),
                address(this)
            );

            _collections[collection_] = syntheticNFTAddress;
        }

        _rentalSellOrders[rentalId] = RentalListing(
            collection_,
            tokenId_,
            maximumRentTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            msg.sender
        );

        emit OfferListed(
            collection_,
            tokenId_,
            maximumRentTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            msg.sender,
            rentalId
        );

    }

    function cancelListOffer(
        address collection_,
        uint256 tokenId_,
        uint256 maximumRentTime_,
        uint256 pricePerSecond_,
        uint256 rentalExpiration_,
        address fundingToken_
    ) public {

        bytes32 rentalListingId = getRentalSellID(
            collection_,
            tokenId_,
            maximumRentTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            msg.sender
        );

        require(_rentalSellOrders[rentalListingId].owner != address(0), "Listing not existent");

        delete _rentalSellOrders[rentalListingId];

        emit OfferCanceled(rentalListingId);
    }


    function rentNFT(
        address collection_,
        uint256 tokenId_,
        uint256 maximumRentTime_,
        uint256 pricePerSecond_,
        uint256 rentalExpiration_,
        address fundingToken_,
        address operator_,
        uint256 rentalTime_
    ) public {

        require(!isTokenRented(collection_, tokenId_), "Token already rented!");

        bytes32 rentalListingId = getRentalSellID(
            collection_,
            tokenId_,
            maximumRentTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            operator_
        );

        RentalListing memory listing = _rentalSellOrders[rentalListingId];

        require(listing.pricePerSecond > 0, "Rental listing not existent");

        IERC721(collection_).transferFrom(
            operator_,
            vaultAddress,
            tokenId_
        );

        Vault(vaultAddress).registerNFT(collection_, tokenId_, listing.owner);

        // Base fee
        uint256 rentalFeeBase = rentalTime_ * pricePerSecond_;

        // What the buyer will pay
        uint256 rentalFeeBuy = rentalFeeBase + rentalFeeBase * ProtocolParameters(_protocolParameters).buyerFee() / 10000;

        require(
            IERC20(fundingToken_).transferFrom(msg.sender, address(this), rentalFeeBuy),
            "Couldn't transfer funds!"
        );

        IERC20(fundingToken_).transfer(
            listing.owner, 
            rentalFeeBase - rentalFeeBase / 10000 * ProtocolParameters(_protocolParameters).sellerFee()
        );

        IERC20(fundingToken_).transfer(
            ProtocolParameters(_protocolParameters).feesRecipient(), 
            (rentalTime_ * pricePerSecond_ / 10000 * ProtocolParameters(_protocolParameters).buyerFee()) + 
            (rentalTime_ * pricePerSecond_ / 10000 * ProtocolParameters(_protocolParameters).sellerFee())
        );

        // Mint synthetic NFT with same metadata

        uint256 syntheticID = ISyntheticNFT(_collections[collection_]).safeMint(
            msg.sender,
            IERC721Metadata(collection_).tokenURI(listing.tokenId),
            rentalTime_
        );

        delete _rentalSellOrders[rentalListingId];

        rentalExpirations[collection_][tokenId_] = block.timestamp + rentalTime_;

        emit NFTRented(
            collection_,
            tokenId_,
            rentalTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            operator_,
            syntheticID
        );

        rentedTokenData[collection_][tokenId_] = RentalListing(
            collection_,
            tokenId_,
            rentalTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            operator_
        );

        rentedTokenSyntheticID[collection_][tokenId_] = syntheticID;
    }

    /**
    * @notice this is a person trying to renti an NFT.
    * He/She gets a synthetic NFT linking to the rental manager
    * which registered this new syntheticNFT and set in status
    * active till the date of invalidating it. The buyer pays
    * the rental in advance.
    */
    function rentalOffer(
        address collection_,
        uint256 tokenId_,
        uint256 rentalTime_,
        uint256 pricePerSecond_,
        uint256 rentalExpiration_,
        address fundingToken_,
        address operator_
    ) public {

        require(rentalTime_ > 0, "Rental time must be greater than zero");
        require(pricePerSecond_ > 0, "Price per second must be greater than zero");
        require(rentalExpiration_ > block.timestamp, "rentalExpiration must be greater than block timestamp!");
        require(whitelistedFundingTokens[fundingToken_], "Funding token must be whitelisted!");

        uint256 allowance = IERC20(fundingToken_).allowance(msg.sender, address(this));

        uint256 fee;

        uint256 buyerFee = ProtocolParameters(_protocolParameters).buyerFee();

        fee = rentalTime_ * pricePerSecond_;
        fee = fee + (fee / 10000 * buyerFee);

        require(allowance >= fee, "Allowance must be as much as fee!");

        bytes32 rentalOfferId = getRentalBuyID(
            msg.sender,
            collection_,
            tokenId_,
            rentalTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            operator_
        );

        _rentalBuyOrders[rentalOfferId] = RentalOffer(
            msg.sender,
            collection_,
            tokenId_,
            rentalTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            operator_
        );

        emit RentalOffered(
            collection_,
            tokenId_,
            rentalTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            operator_,
            rentalOfferId
        );

    }

    function cancelRentalOffer(
        address collection_,
        uint256 tokenId_,
        uint256 rentalTime_,
        uint256 pricePerSecond_,
        uint256 rentalExpiration_,
        address fundingToken_,
        address operator_
    ) public {
        bytes32 rentalOfferId = getRentalBuyID(
            msg.sender,
            collection_,
            tokenId_,
            rentalTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            operator_
        );

        _cancelRentalOfferById(rentalOfferId);
    }

    function _cancelRentalOfferById(
        bytes32 rentalOfferId_
    ) internal {
        RentalOffer memory offer = _rentalBuyOrders[rentalOfferId_];
        require(offer.pricePerSecond > 0, "Offer not existent");
        delete _rentalBuyOrders[rentalOfferId_];

        emit RentalOfferCanceled(rentalOfferId_);
    }

    /**
    * @notice here the owner of the NFT accepts the rental of the NFT.
    * The rental price is transfered to owner and the syntethic is minted to buyer.
     */
    function acceptRentalOffer(
        address collection_,
        uint256 tokenId_,
        uint256 rentalTime_,
        uint256 pricePerSecond_,
        uint256 rentalExpiration_,
        address fundingToken_,
        address offerer_
    ) public {

        bytes32 rentalOfferId = getRentalBuyID(
            offerer_,
            collection_,
            tokenId_,
            rentalTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            msg.sender
        );
        
        require(_rentalBuyOrders[rentalOfferId].pricePerSecond > 0, "Rental offer not existent");

        require(!isTokenRented(collection_, tokenId_), "token already rented!");

        address approved = IERC721Metadata(collection_).getApproved(tokenId_);
        
        require(approved == address(this), "Token must be approved");
   
        if (_collections[collection_] == address(0)) {
            //Deploy rentalSyntheticNFT

            IERC721Metadata originalCollection = IERC721Metadata(collection_);

            address syntheticNFTAddress = Clones.clone(_syntheticNFT);

            string memory originalName = originalCollection.name();

            string memory originalSymbol = originalCollection.symbol();

            // initializes the SyntheticNFT
            SyntheticNFT(syntheticNFTAddress).initialize(
                string(abi.encodePacked("Rented ", originalName)),
                string(abi.encodePacked("r", originalSymbol)),
                address(this)
            );

            _collections[collection_] = syntheticNFTAddress;
        }

        IERC721(collection_).transferFrom(
            msg.sender,
            vaultAddress,
            tokenId_
        );

        Vault(vaultAddress).registerNFT(collection_, tokenId_, msg.sender);

        // Transfer from offerer to contract
        require(
            IERC20(fundingToken_).transferFrom(
                _rentalBuyOrders[rentalOfferId].offerer,
                address(this),
                rentalTime_ * pricePerSecond_ +
                (
                    rentalTime_ * pricePerSecond_ 
                    / 10000 * ProtocolParameters(_protocolParameters).buyerFee())
            ),
            "Couldn't transfer funds!"
        );

        require(
            IERC20(fundingToken_).transfer(
                msg.sender, 
                rentalTime_ * pricePerSecond_ -
                (rentalTime_ * pricePerSecond_ / 10000 * ProtocolParameters(_protocolParameters).sellerFee())
            ),
            "Couldn't transfer funds!"
        ); 

        require(
            IERC20(fundingToken_).transfer(
                ProtocolParameters(_protocolParameters).feesRecipient(), 
                (rentalTime_ * pricePerSecond_ / 10000 * ProtocolParameters(_protocolParameters).buyerFee()) +
                (rentalTime_ * pricePerSecond_ / 10000 * ProtocolParameters(_protocolParameters).sellerFee())
            ),
            "Couldn't transfer funds!"
        ); 

        // Mint synthetic NFT with same metadata
        uint256 syntheticID = ISyntheticNFT(_collections[collection_]).safeMint(
            _rentalBuyOrders[rentalOfferId].offerer,
            IERC721Metadata(collection_).tokenURI(tokenId_),
            _rentalBuyOrders[rentalOfferId].rentalTime
        );

        delete _rentalBuyOrders[rentalOfferId];

        rentalExpirations[collection_][tokenId_] = block.timestamp + rentalTime_;

        emit NFTRented(
            collection_,
            tokenId_,
            rentalTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            msg.sender,
            syntheticID
        );

        rentedTokenData[collection_][tokenId_] = RentalListing(
            collection_,
            tokenId_,
            rentalTime_,
            pricePerSecond_,
            rentalExpiration_,
            fundingToken_,
            msg.sender
        );

        rentedTokenSyntheticID[collection_][tokenId_] = syntheticID;

    }

    function getRentalSellID(
        address collection_,
        uint256 tokenId_,
        uint256 maximumRentTime_,
        uint256 pricePerSecond_,
        uint256 rentalExpiration_,
        address fundingToken_,
        address operator_
    ) public pure returns(bytes32 rentalId) {
        rentalId = keccak256(
            abi.encode(
                collection_,
                tokenId_,
                maximumRentTime_,
                pricePerSecond_,
                rentalExpiration_,
                fundingToken_,
                operator_
            )
        );
    }

    function getRentalBuyID(
        address offerer_,
        address collection_,
        uint256 tokenId_,
        uint256 rentalTime_,
        uint256 pricePerSecond_,
        uint256 rentalExpiration_,
        address fundingToken_,
        address operator_
    ) public pure returns(bytes32 rentalId) {
        rentalId = keccak256(
            abi.encode(
                offerer_,
                collection_,
                tokenId_,
                rentalTime_,
                pricePerSecond_,
                rentalExpiration_,
                fundingToken_,
                operator_
            )
        );
    }

    function getSyntheticNFTOwner(address collection_, uint256 tokenId_) public view returns(address) {
        return SyntheticNFT(_collections[collection_]).ownerOf(tokenId_);
    }

    function getSyntheticNFTAddress(address collection_) public view returns (address) {
        return _collections[collection_];
    }

    function syntheticNFTExists(address collection_, uint256 tokenId_) public view returns (bool) {
        return SyntheticNFT(_collections[collection_]).exists(tokenId_);
    }

    function isRentalExpired(address collection_, uint256 tokenId_) public view returns (bool) {
        return SyntheticNFT(_collections[collection_]).isRentalExpired(tokenId_);
    }

    function isTokenRented(address collection, uint256 tokenId) public view returns (bool) {
        return rentalExpirations[collection][tokenId] > block.timestamp;
    }

    function _authorizeUpgrade(address) internal view override onlyOwner {}

    function getSyntheticID(address collection, uint256 tokenId) public view returns(uint256) {
        return rentedTokenSyntheticID[collection][tokenId];
    }

}
