// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;
import "@openzeppelin/contracts/token/ERC721/extensions/IERC721Metadata.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

interface ISyntheticNFT is IERC721Metadata {
    function setMetadata(uint256 tokenId, string memory metadata) external;

    function isVerified(uint256 tokenId) external view returns (bool);

    function exists(uint256 tokenId) external view returns (bool);

    function safeMint(address to, string memory metadata, uint256 lifetime) external returns (uint256);

    function safeBurn(uint256 tokenId) external;

    function isRentalExpired(uint256 tokenId) external view returns (bool);
}

interface IVault {
    function isTokenInVault(
        address operator, 
        uint256 tokenId
    ) external view returns (bool);

    function originalOwner(
        address operator, 
        uint256 tokenId
    ) external view returns(address);
}