// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;

import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./RentalManager.sol";

contract Vault is Initializable, IERC721Receiver {

    using Counters for Counters.Counter;

    Counters.Counter private _bidCounter;

    /* ****** */
    /* EVENTS */
    /* ****** */

    event NFTReceived(
        address operator,
        address from,
        uint256 tokenId,
        bytes data
    );

    event NFTWithdrawn(
        address collection,
        uint256 tokenId,
        address to
    );

    /* ******* */
    /* STRUCTS */
    /* ******* */

    // State variables
    /// @notice The current owner of the vault.
    address public owner;

    // NFT address => NFT ID => Original Owner
    mapping (address => mapping (uint256 => address) ) public tokenData;

    /**
    * @dev Throws if called by any account other than the owner.
    */
    modifier onlyOwner {
        require(msg.sender == owner, "Ownable: caller is not the owner");
        _;
    }

    // solhint-disable-next-line
    constructor(
        address _owner
    ) {
        owner = _owner;
    }

    /**
     * @param _owner the owner of the Vault
     */
    function initialize(
        address _owner
    ) external initializer {
        owner = _owner;
    }

    /**
    * @dev See {IERC721Receiver-onERC721Received}.
    */
    function onERC721Received(
        address operator_,
        address from_,
        uint256 tokenId_,
        bytes memory data_
    ) public virtual override returns (bytes4) {
        tokenData[msg.sender][tokenId_] = operator_;

        emit NFTReceived(operator_, from_, tokenId_, data_);

        return this.onERC721Received.selector;
    }

    function isTokenInVault(address collection_, uint256 tokenId_) public view returns (bool) {
        address previousOwner = tokenData[collection_][tokenId_];
        if (previousOwner != address(0)) {
            return true;
        } else {
            return false;
        }
    }

    function originalOwner(address collection_, uint256 tokenId_) public view returns(address) {
        return tokenData[collection_][tokenId_];
    }

    function withdraw(address collection_, uint256 tokenId_) public {
        require(isTokenInVault(collection_, tokenId_), "Token not in vault");
        require(!RentalManager(owner).isTokenRented(collection_, tokenId_), "Can't withdraw, token is rented!");
        require(tokenData[collection_][tokenId_] == msg.sender, "you are not the owner!");

        IERC721(collection_).transferFrom(address(this), msg.sender, tokenId_);
        tokenData[collection_][tokenId_] = address(0);

        emit NFTWithdrawn(collection_, tokenId_, msg.sender);
    }

    function registerNFT(address collection_, uint256 tokenId_, address owner_) public onlyOwner {
        require(
            IERC721(collection_).ownerOf(tokenId_) == address(this),
            "Vault is not owner of NFT"
        );

        tokenData[collection_][tokenId_] = owner_;

        emit NFTReceived(collection_, owner_, tokenId_, '');
    }
}
