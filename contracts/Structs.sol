// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;

struct RentalListing {
    address collection;
    uint256 tokenId;
    uint256 maximumRentTime;
    uint256 pricePerSecond;
    uint256 rentalExpiration;
    address fundingToken;
    address owner;
}

struct RentalOffer{
    address offerer;
    address collection;
    uint256 tokenId;
    uint256 rentalTime;
    uint256 pricePerSecond;
    uint256 rentalExpiration;
    address fundingToken;
    address from;
}
