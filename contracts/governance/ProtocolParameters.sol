// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;

import "@openzeppelin/contracts/access/Ownable.sol";

/**
 * @title parameters controlled by governance
 * @notice the owner of this contract is the timelock controller of the governance feature
 */
contract ProtocolParameters is Ownable {
    uint256 public buyerFee;
    
    uint256 public sellerFee;

    address public feesRecipient;

    event BuyerFeeUpdated(uint256 from, uint256 to);
    event SellerFeeUpdated(uint256 from, uint256 to);
    event FeesRecipientUpdated(address from, address to);

    constructor(
        uint256 buyerFee_,
        uint256 sellerFee_,
        address feesRecipient_//,
        //address governanceContractAddress_,
    ) {
        require(buyerFee_ > 0, "Invalid buyer fee");
        require(sellerFee_ > 0, "Invalid seller fee");
        require(feesRecipient_ != address(0), "Invalid fees recipient address");

        buyerFee = buyerFee_;
        sellerFee_ = sellerFee;
        feesRecipient = feesRecipient_;

        // transfer ownership
        //transferOwnership(governanceContractAddress_);
    }

    function setBuyerFee(uint256 buyerFee_) external onlyOwner {
        require(buyerFee_ > 0, "Invalid buyer fee");
        emit BuyerFeeUpdated(buyerFee, buyerFee_);
        buyerFee = buyerFee_;
    }

    function setSellerFee(uint256 sellerFee_) external onlyOwner {
        require(sellerFee_ > 0, "Invalid seller fee");
        emit SellerFeeUpdated(sellerFee, sellerFee_);
        sellerFee = sellerFee_ ;
    }

    function setFeesRecipient(address feesRecipient_) external onlyOwner {
        require(feesRecipient_ != address(0), "Invalid fees recipient");
        emit FeesRecipientUpdated(feesRecipient, feesRecipient_);
        feesRecipient = feesRecipient_;
    }

}