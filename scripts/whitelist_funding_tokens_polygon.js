const { expect } = require('chai');
const { ethers, network } = require('hardhat');
fs = require('fs');

async function asyncCall() {

    [owner] = await ethers.getSigners();

    const RentalManager = await ethers.getContractAt(
        'RentalManager', 
        '0x8435Ab8E144CDeD98a2D6386D00e6294C6948130'
    );

    await RentalManager.whitelistFundingToken('0xc2132d05d31c914a87c6611c10748aeb04b58e8f');

}

asyncCall();
