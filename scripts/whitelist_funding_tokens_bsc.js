const { expect } = require('chai');
const { ethers, network } = require('hardhat');
fs = require('fs');

async function asyncCall() {

    [owner] = await ethers.getSigners();

    const RentalManager = await ethers.getContractAt(
        'RentalManager', 
        '0xA9444E4c49BC3f01D63279e3bd6b8Ae75fb6A252'
    );

    await RentalManager.whitelistFundingToken('0x5b511cbe1f424ccd2f627698624561107bb0d97d');

}

asyncCall();
