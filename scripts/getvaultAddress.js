const { expect } = require('chai');
const { ethers, network } = require('hardhat');
fs = require('fs');

async function asyncCall() {

    [owner] = await ethers.getSigners();

    const RentalManager = await ethers.getContractAt(
        'RentalManager', 
        '0x6ad53e256Bdc099Bc04Cd091ABa7f1f0F444167b'
    );

    const whitelisted = await RentalManager.whitelistedFundingTokens('0x5b511cbe1f424ccd2f627698624561107bb0d97d');
    console.log('whitelisted', whitelisted);

    await RentalManager.whitelistFundingToken('0x5b511cbe1f424ccd2f627698624561107bb0d97d');

}

asyncCall();
