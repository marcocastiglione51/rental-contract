const { expect } = require('chai');
const { ethers, network } = require('hardhat');
fs = require('fs');

async function asyncCall() {

    const name = "Cool Cats";
    const symbol = "COOL";

    [owner] = await ethers.getSigners();

    const RentalNFT = await ethers.getContractFactory("RentalNFT");
    const rentalNFT = await RentalNFT.deploy(name, symbol);

    for (let i = 0; i < 10000; i++) {
        var text = 'https://api.coolcatsnft.com/cat/' + i;

        await rentalNFT.safeMint(owner.address, text, i);
        console.log('owner.address', owner.address);
        console.log('text', text);
        console.log('i', i);
        console.log('rentalNFT.address', rentalNFT.address);

    }

}

asyncCall();
