module.exports = async ({ getNamedAccounts, deployments, network }) => {
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  if (network.tags.local || network.tags.testnet) {
    await deploy('MockNFT', {
      from: deployer,
      log: true,
      args: ['Rental Mock NFT', 'MNFT'],
    });
  }
};

module.exports.tags = ['nft_mock'];
