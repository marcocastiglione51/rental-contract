module.exports = async ({ getNamedAccounts, deployments }) => {
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // this contract is upgradeable through uups (EIP-1822)
  await deploy('SyntheticNFT', {
    from: deployer,
    log: true,
    args: [],
  });
};


module.exports.tags = ['synthetic_nft'];
