module.exports = async ({ getNamedAccounts, deployments, network }) => {
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  console.log("deployer", deployer);

  const Vault = await ethers.getContract('Vault');
  const SyntheticNFT = await ethers.getContract('SyntheticNFT');
  const ProtocolParameters = await ethers.getContract('ProtocolParameters');

  // this contract is upgradeable through uups (EIP-1822)
  let RentalManager = await deploy('RentalManager', {
    from: deployer,
    log: true,
    proxy: {
      proxyContract: 'UUPSProxy',
      execute: {
        init: {
          methodName: 'initialize', // method to be executed when the proxy is deployed
          args: [
            SyntheticNFT.address, 
            Vault.address,
            ProtocolParameters.address
          ],
        },
      },
    },

    args: [],
  });

  await Vault.initialize(
    RentalManager.address
  )

};

module.exports.dependencies = [
  'vault',
  'synthetic_nft',
  'usdt_mock',
  'protocol_parameters'
]

module.exports.tags = ['rental_manager'];
