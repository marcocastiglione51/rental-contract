module.exports = async ({ getNamedAccounts, deployments }) => {
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // this contract is upgradeable through uups (EIP-1822)
  await deploy('ProtocolParameters', {
    from: deployer,
    log: true,
    args: [250, 250, deployer],
  });
};


module.exports.tags = ['protocol_parameters'];
