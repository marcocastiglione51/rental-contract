module.exports = async ({ getNamedAccounts, deployments }) => {
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // this contract is upgradeable through uups (EIP-1822)
  await deploy('Vault', {
    from: deployer,
    log: true,
    args: [deployer],
  });
};

module.exports.tags = ['vault'];
