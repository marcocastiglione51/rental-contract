const { assert, expect } = require('chai');
const { expectEvent, expectRevert, BN } = require('@openzeppelin/test-helpers');
const { ethers } = require('hardhat');
const { getEventArgs } = require('./helpers/events');

describe('RentalManager', async function () {

  beforeEach(async () => {
    // Using fixture from hardhat-deploy

    [owner, addr1, addr2] = await ethers.getSigners();

    const namedAccounts = await getNamedAccounts();
    
    deployer = namedAccounts.deployer;
    user = namedAccounts.deployer;

    await deployments.fixture(
      [
        'rental_manager', 
        'vault', 
        'synthetic_nft', 
        'synthetic_nft',
        'nft_mock'
      ]
    );

    rentalmanager = await ethers.getContract('RentalManager');
    vaultAddress  = await rentalmanager.vaultAddress();
    vault         = await ethers.getContractAt('Vault', vaultAddress);
    mockusdt      = await ethers.getContract('MockUSDT');
    synthetic_nft = await ethers.getContract('SyntheticNFT');
    nft_mock      = await ethers.getContract('MockNFT');

    await nft_mock.safeMint(
      deployer,
      ''
    );

    await mockusdt.transfer(user, 10000000);

    await rentalmanager.whitelistFundingToken(mockusdt.address);

  });

  it('should be deployed', async () => {
    assert.ok(rentalmanager.address);
    assert.ok(vault.address);
    assert.ok(mockusdt.address);
    assert.ok(synthetic_nft.address);
    assert.ok(nft_mock.address);
  });

  describe('listOffer', () => {
    it('NFT not approved', async () => {
      await expect(
        rentalmanager.listOffer(
          nft_mock.address,
          0, 
          1, 
          10,
          2637383030,
          mockusdt.address
        )
      ).to.be.revertedWith('Token must be approved');
    });

    it('maximumRentTime 0 or less', async () => {
      await expect(
        rentalmanager.listOffer(
          nft_mock.address,
          0, 
          0, 
          10,
          2637383030,
          mockusdt.address
        )
      ).to.be.revertedWith('Rent time must be greater than zero');
    });

    it('pricePerSecond 0 or less', async () => {
      await expect(
        rentalmanager.listOffer(
          nft_mock.address,
          0, 
          1, 
          0,
          2637383030,
          mockusdt.address
        )
      ).to.be.revertedWith('pricePerSecond must be greater than zero');
    });

    it('Rental expiration too low', async () => {
      await expect(
        rentalmanager.listOffer(
          nft_mock.address,
          0, 
          1, 
          10,
          1,
          mockusdt.address
        )
      ).to.be.revertedWith('rentalExpiration must be greater than block timestamp!');
    });

    it('Try to withdraw rented NFT', async () => {
    
      await nft_mock.approve(rentalmanager.address, 0);
      
      await rentalmanager.listOffer(
        nft_mock.address,
        0, 
        100, 
        10,
        2637383030,
        mockusdt.address
      )

      await mockusdt.approve(rentalmanager.address, 1025);

      await rentalmanager.rentNFT(
        nft_mock.address,
        0, 
        100, 
        10,
        2637383030,
        mockusdt.address,
        deployer,
        100

      );

      await expect(vault.withdraw(nft_mock.address, 0)).to.be.revertedWith(
        "Can't withdraw, token is rented!"
      );

    });

    it('Invalid collection', async () => {
    
      await nft_mock.approve(rentalmanager.address, 0);
      
      await expect(rentalmanager.listOffer(
        vault.address,
        0, 
        100, 
        10,
        2637383030,
        mockusdt.address
      )).to.be.reverted;

    });

  });

  describe('cancelListOffer', () => {
    it('Offer not existent', async () => {
      await expect(rentalmanager.cancelListOffer(
        nft_mock.address,
        0, 
        1, 
        10,
        2637383030,
        mockusdt.address
      )).to.be.revertedWith('Listing not existent');
    });

    it('Wrong address', async () => {

      await nft_mock.approve(rentalmanager.address, 0);
      
      await rentalmanager.listOffer(
        nft_mock.address,
        0, 
        100, 
        10,
        2637383030,
        mockusdt.address
      )

      await mockusdt.approve(rentalmanager.address, 1000);

      await expect(rentalmanager.connect(addr1).cancelListOffer(
        nft_mock.address,
        0, 
        100, 
        10,
        2637383030,
        mockusdt.address
      )).to.be.revertedWith('Listing not existent');
    });
  });

  describe('cancelRentalOffer', () => {
    it('Offer not existent', async () => {
      await expect(rentalmanager.cancelRentalOffer(
        nft_mock.address,
        0, 
        1, 
        10,
        2637383030,
        mockusdt.address,
        deployer
      )).to.be.revertedWith('Offer not existent');
    });

  });


  describe('rentNFT', () => {
    it('Offer not existent', async () => {
      await expect(rentalmanager.rentNFT(
        nft_mock.address,
        0, 
        1, 
        10,
        2637383030,
        mockusdt.address,
        deployer,
        1
      )).to.be.revertedWith('Rental listing not existent');
    });

    it('Allowance not enough', async () => {
    
      await nft_mock.approve(rentalmanager.address, 0);
      
      await rentalmanager.listOffer(
        nft_mock.address,
        0, 
        100, 
        10,
        2637383030,
        mockusdt.address
      );

      await expect(rentalmanager.rentNFT(
        nft_mock.address,
        0, 
        100, 
        10,
        2637383030,
        mockusdt.address,
        deployer,
        100
      )).to.be.revertedWith('ERC20: transfer amount exceeds allowance');
    });
  });

  describe('rentalOffer', () => {

    it('Allowance must be as much as fee!', async () => {

      await mockusdt.approve(rentalmanager.address, 9);

      await expect(rentalmanager.rentalOffer(
        nft_mock.address,
        0,
        1,
        10,
        2637383030,
        mockusdt.address,
        deployer
      )).to.be.revertedWith("Allowance must be as much as fee!");

    });

    it('Rental time must be greater than zero', async () => {

      await expect(rentalmanager.rentalOffer(
        nft_mock.address,
        0,
        0,
        10,
        2637383030,
        mockusdt.address,
        deployer
      )).to.be.revertedWith('Rental time must be greater than zero');

    });

    it('Price per second must be greater than zero', async () => {

      await expect(rentalmanager.rentalOffer(
        nft_mock.address,
        0,
        1,
        0,
        2637383030,
        mockusdt.address,
        deployer
      )).to.be.revertedWith('Price per second must be greater than zero');

    });

    it('Rental expiration must be greater than block timestamp!', async () => {

      await expect(
        rentalmanager.rentalOffer(
          nft_mock.address,
          0, 
          1, 
          10,
          1,
          mockusdt.address,
          deployer
        )
      ).to.be.revertedWith('rentalExpiration must be greater than block timestamp!');

    });

    it('Ok', async () => {

      await mockusdt.approve(rentalmanager.address, 1025); 

      await rentalmanager.rentalOffer(
          nft_mock.address,
          0, 
          1, 
          10,
          2637383030,
          mockusdt.address,
          deployer
      );

      await nft_mock.approve(rentalmanager.address, 0);

      await rentalmanager.acceptRentalOffer(
          nft_mock.address,
          0, 
          1, 
          10,
          2637383030,
          mockusdt.address,
          deployer
      );

    });

  });

  describe('acceptRentalOffer', () => {

    it('Token must be approved', async () => {

      await mockusdt.approve(rentalmanager.address, 10);

      await rentalmanager.rentalOffer(
        nft_mock.address,
        0,
        1,
        10,
        2637383030,
        mockusdt.address,
        deployer
      );

      await expect(rentalmanager.acceptRentalOffer(
        nft_mock.address,
        0,
        1,
        10,
        2637383030,
        mockusdt.address,
        deployer
      )).to.be.revertedWith('Token must be approved');

    });

    it('Rental offer not existent', async () => {

      await expect(rentalmanager.acceptRentalOffer(
        nft_mock.address,
        0,
        1,
        10,
        2637383030,
        mockusdt.address,
        deployer
      )).to.be.revertedWith('Rental offer not existent');

    });


  });

  describe('Full flow', () => {
    it('Full flow', async () => {
        
        await nft_mock.approve(rentalmanager.address, 0);
      
        
        await rentalmanager.listOffer(
          nft_mock.address,
          0, 
          1, 
          10,
          2637383030,
          mockusdt.address
        )

        await mockusdt.approve(rentalmanager.address, 10);
        
        await rentalmanager.rentNFT(
          nft_mock.address,
          0, 
          1, 
          10,
          2637383030,
          mockusdt.address,
          deployer,
          1
        );

        const SyntheticNFTAddress = await rentalmanager.getSyntheticNFTAddress(
          nft_mock.address
        );

        const syntheticNFTExists = await rentalmanager.syntheticNFTExists(nft_mock.address, 0);
        const rentalExpired = await rentalmanager.isRentalExpired(nft_mock.address, 0)

        expect(syntheticNFTExists).to.be.equal(true);
        expect(rentalExpired).to.be.equal(false);
    });
  });

});
