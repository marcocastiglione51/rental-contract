 const { assert, expect } = require('chai');
const { expectEvent, expectRevert, BN } = require('@openzeppelin/test-helpers');
const { ethers } = require('hardhat');
const { getEventArgs } = require('./helpers/events');

describe('Vault', async function () {

  beforeEach(async () => {
    // Using fixture from hardhat-deploy

    [owner, addr1, addr2] = await ethers.getSigners();

    const namedAccounts = await getNamedAccounts();
    
    deployer = namedAccounts.deployer;

    await deployments.fixture(
      [
        'rental_manager', 
        'vault',
        'nft_mock'
      ]
    );

    rentalmanager = await ethers.getContract('RentalManager');
    const vaultAddress  = await rentalmanager.vaultAddress();
    vault         = await ethers.getContractAt('Vault', vaultAddress);
    mockusdt      = await ethers.getContract('MockUSDT');
    nft_mock      = await ethers.getContract('MockNFT');

    await nft_mock.safeMint(
      deployer,
      ''
    );

    await rentalmanager.whitelistFundingToken(mockusdt.address);

  });

  it('should be deployed', async () => {
    assert.ok(rentalmanager.address);
    assert.ok(vault.address);
    assert.ok(mockusdt.address);
    assert.ok(nft_mock.address);
  });

  describe('isTokenInVault', () => {
    it('NFT not existent', async () => {
      const isTokenInVault = await vault.isTokenInVault(
          nft_mock.address,
          0
        );

      await expect(isTokenInVault).to.be.equal(false);
    });

    it('Ok', async () => { 

      await nft_mock.approve(rentalmanager.address, 0);

      await rentalmanager.listOffer(
        nft_mock.address,
        0, 
        1, 
        10,
        2637383030,
        mockusdt.address
      )

      await mockusdt.approve(rentalmanager.address, 10);

      await rentalmanager.rentNFT(
        nft_mock.address,
        0, 
        1,
        10,
        2637383030,
        mockusdt.address,
        deployer,
        1
      );

      const isTokenInVault = await vault.isTokenInVault(
        nft_mock.address,
        0
      );

      await expect(isTokenInVault).to.be.equal(true);

    });

    
    it('After withdraw', async () => { 

      await nft_mock.approve(rentalmanager.address, 0);

      await rentalmanager.listOffer(
        nft_mock.address,
        0, 
        1, 
        10,
        2637383030,
        mockusdt.address
      )

      await mockusdt.approve(rentalmanager.address, 10);

      await rentalmanager.rentNFT(
        nft_mock.address,
        0, 
        1,
        10,
        2637383030,
        mockusdt.address,
        deployer,
        1
      );

      await vault.withdraw(nft_mock.address, 0);

      const isTokenInVaultAfter = await vault.isTokenInVault(
        nft_mock.address,
        0
      );

      await expect(isTokenInVaultAfter).to.be.equal(false);

    });
  });

  describe('withdraw', () => {
    it('Token not in vault', async () => {
      await expect(
        vault.withdraw(
          nft_mock.address,
          0
        )
      ).to.be.revertedWith('Token not in vault');
    });
      
    it("Can't withdraw, token is rented!", async () => {

      await nft_mock.approve(rentalmanager.address, 0);

      await rentalmanager.listOffer(
        nft_mock.address,
        0, 
        10, 
        10,
        2637383030,
        mockusdt.address
      )

      await mockusdt.approve(rentalmanager.address, 102);

      await rentalmanager.rentNFT(
        nft_mock.address,
        0, 
        10, 
        10,
        2637383030,
        mockusdt.address,
        deployer,
        10
      );

      await expect(
        vault.withdraw(
          nft_mock.address,
          0
        )
      ).to.be.revertedWith("Can't withdraw, token is rented!");
    });

    it("you are not the owner!", async () => {

      await nft_mock.approve(rentalmanager.address, 0);

      await rentalmanager.listOffer(
        nft_mock.address,
        0, 
        1, 
        1,
        2637383030,
        mockusdt.address
      )

      await mockusdt.approve(rentalmanager.address, 1);

      await rentalmanager.rentNFT(
        nft_mock.address,
        0, 
        1, 
        1,
        2637383030,
        mockusdt.address,
        deployer,
        1
      );

      await expect(
        vault.connect(addr1).withdraw(
          nft_mock.address,
          0
        )
      ).to.be.revertedWith("you are not the owner!");
    });
  });

});
